<?php
use kimlu\environment\Env;
use kimlu\servers\UIServer;
use kimlu\servers\modules\FileDispatchModule;

require '../vendor/autoload.php';

$pathEnv = dirname( __DIR__ ).DIRECTORY_SEPARATOR;

Env::start( $pathEnv, 'test/env.json' );

UIServer::start( 'test/ui-server.json' );
UIServer::execute();