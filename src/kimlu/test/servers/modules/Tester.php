<?php
namespace kimlu\test\servers\modules;

use stdClass;
use kimlu\servers\UIServer;
use kimlu\servers\commons\ModuleRegistered;
use kimlu\utiles\UUID;

abstract class Tester extends ModuleRegistered
{
    /**
     * 
     * @param array $args
     */
    static public function echo( array $args )
    {
        $response = new stdClass();
        $response->classname = __CLASS__;
        $response->timestamp = microtime();
        $response->uuid = UUID::generate();
        UIServer::response()->content( $response );
    }    
}
?>