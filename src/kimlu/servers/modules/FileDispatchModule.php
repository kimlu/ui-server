<?php
namespace kimlu\servers\modules;

use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use kimlu\core\cache\CacheManager;
use kimlu\servers\UIServer;
use kimlu\servers\commons\Module;
use kimlu\servers\commons\cmd\CmdException;
use kimlu\utiles\Path;
use kimlu\utiles\Text;

/**
 *
 * @author Máximo
 *        
 */
abstract class FileDispatchModule extends Module
{
    
    /**
     * 
     * @var CacheManager
     */
    static private $fileManager = null;
    
    /**
     * 
     * @var array
     */
    static private $fileMap = NULL;
    
    /**
     * 
     * @param string $directoryRootFullPath
     */
    static public function connect( string $directoryRootFullPath )
    {
        self::$fileManager = new CacheManager( $directoryRootFullPath );
        self::sincronizeMap();
    }
    
    /**
     * 
     * @return CacheManager
     */
    static public function fileManager() : CacheManager
    {
        return self::$fileManager;
    }
    
    /**
     * 
     * @param string $relativePath
     * @return string
     */
    static public function fullPathSafe( string $relativePath ): string
    {
        $relativePath = Path::safe( $relativePath );
        if ( Text::create( $relativePath )->startWith( DIRECTORY_SEPARATOR ) )
        {
            $relativePath = substr( $relativePath, 1 );
        }
        return Path::safe( self::fileManager()->directoryFullPath().$relativePath );
    }
    
    /**
     * 
     */
    static public  function sincronizeMap() 
    {
        self::$fileMap = [];
        $iterator = new RecursiveIteratorIterator( 
                    new RecursiveDirectoryIterator( 
                            self::$fileManager->directoryFullPath() 
                        ) 
                );
        foreach ( $iterator as $file )
        {
            if ( $file->isDir() )
            {
                continue;
            }
            $fileFullPath = Path::safe( $file->getPathname() );
            $fileRelativePath = substr( $fileFullPath, strlen( self::$fileManager->directoryFullPath() ) );
            $fileKey = str_replace( DIRECTORY_SEPARATOR, '/', $fileRelativePath );
            self::$fileMap[ $fileKey ] = $fileRelativePath;
        }
    }
    
    /**
     * 
     * @param string $fileKey
     */
    static public function get( array $args )
    {
        if ( isset( $args[ 0 ] ) )
        {
            $fileKey = $args[ 0 ];
            if ( isset( self::$fileMap[ $fileKey ] ) )
            {
                UIServer::response()->fileRelativePath( self::$fileMap[ $fileKey ] );
            }
            else 
            {
                throw new CmdException( 'file', "Error: File key<{$fileKey}> not found" );
            }
        }
        else
        {
            throw new CmdException( 'args', "Error: first argument empty." );
        }
    }
    
}