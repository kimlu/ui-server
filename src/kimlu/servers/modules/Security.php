<?php
namespace kimlu\servers\modules;

use DateInterval;
use DateTime;
use Throwable;
use stdClass;
use kimlu\servers\UIServer;
use kimlu\servers\commons\Module;
use kimlu\servers\commons\ModuleRegistered;
use kimlu\servers\commons\cmd\CmdException;
use kimlu\servers\entities\OAuthEntity;
use kimlu\servers\entities\SecurityEntity;
use kimlu\utiles\UUID;

abstract class Security extends Module
{
    /**
     * 
     * @var string
     */
    const HTTP_HEADER_TOKEN = 'HTTP_KIMLU_TOKEN';
    
    /**
     *
     * @var string
     */
    const HTTP_HEADER_TICKET = 'HTTP_KIMLU_TICKET';
    
    /**
     * 
     * @var string
     */
    const INVALID_KEY= 'Security.invalid';
    
    /**
     * 
     * @var SecurityEntity
     */
    static private $entity = null;
    
    /**
     * 
     * @var OAuthEntity
     */
    static private $oauth = null;
    
    /**
     * 
     * @return SecurityEntity
     */
    static public function entity() : SecurityEntity
    {
        $fileSecurity = FileDispatchModule::fullPathSafe( 'security.json' );
        if ( !FileDispatchModule::fileManager()->exist( 'security.json' ) )
        {
            self::$entity = new SecurityEntity( $fileSecurity, TRUE );
            self::$entity->sincronize();
        }
        if ( is_null( self::$entity ) )
        {
            self::$entity = new SecurityEntity( $fileSecurity );
        }
       return self::$entity;
    }

    /**
     * 
     * @return OAuthEntity
     */
    static private function oauth() : OAuthEntity
    {
        $fileOAuth = FileDispatchModule::fullPathSafe( 'oauth.json' );
        if ( !FileDispatchModule::fileManager()->exist( 'oauth.json' ) )
        {
            self::$oauth = new OAuthEntity( $fileOAuth, TRUE );
            self::$oauth->key( '' );
            self::$oauth->sincronize();
        }
        if ( is_null( self::$oauth ) )
        {
            self::$oauth = new OAuthEntity( $fileOAuth );
        }
        return self::$oauth;
    }
    
    /**
     * @param string $id
     * @param string $key
     */
    static public function connect( array $args )
    {
        if ( isset( $args[ 0 ] ) )
        {
            if( $args[ 0 ] == self::oauth()->key() )
            {
                self::entity()->token( UUID::generate() );
                self::entity()->ticket()->token( UUID::generate() );
                Security::refreshTiket( false );
                
                $out = new stdClass();
                $out->token = self::entity()->token();
                $out->ticket = self::entity()->ticket()->token();
                UIServer::response()->content( $out );
            }
            else
            {
                throw new CmdException( self::INVALID_KEY, "Error: Invalid key." );
            }
        }
        else
        {
            throw new CmdException( self::INVALID_KEY, "Error: Empty argument" );
        }
    }
    
    /**
     * 
     * @param array $args
     * @throws CmdException
     */
    static public function disconnect( array $args )
    {
        if ( isset( $args[ 0 ] ) )
        {
            $tokenClient = $args[ 0 ];
            if ( $tokenClient == self::entity()->token().self::entity()->ticket()->token() )
            {
                self::entity()->invalidate();
            }
            else
            {
                throw new CmdException( self::INVALID_KEY, "Error: Key to disconnect invalid" );
            }
        }
        else 
        {
            self::entity()->invalidate();
        }
    }
    
    /**
     * 
     * @throws CmdException
     * @return bool
     */
    static public function isConnected() : bool
    {
        try 
        {
            // caso: CMD request connect client
            if( UIServer::request()->cmd() == get_called_class().'::connect' )
            {
                return true;
            }
            // caso: CMD request disconnect client
            if( UIServer::request()->cmd() == get_called_class().'::disconnect' )
            {
                return true;
            }
            // caso: CMD valid
            if ( isset( $_SERVER[ self::HTTP_HEADER_TOKEN ] ) )
            {
                if ( $_SERVER[ self::HTTP_HEADER_TOKEN ] == self::entity()->token() )
                {
                    if ( is_subclass_of( UIServer::request()->classname(), ModuleRegistered::class ) )
                    {
                        if ( isset( $_SERVER[ self::HTTP_HEADER_TICKET ] ) )
                        {
                            if ( $_SERVER[ self::HTTP_HEADER_TICKET ] == self::entity()->ticket()->token() )
                            {
                                $now = new DateTime( 'now' );
                                $expire = new DateTime( self::entity()->ticket()->expire() );
                                if ( $expire <= $now )
                                {
                                    throw new CmdException( self::INVALID_KEY, "Error: Tiket expired." );
                                }
                            }
                            else
                            {
                                throw new CmdException( self::INVALID_KEY, "Error: Tiket invalid." );
                            }
                        }
                        else
                        {
                            throw new CmdException( self::INVALID_KEY, "Error: Header Ticket not found in request." );
                        }
                        self::refreshTiket();
                    }
                    else 
                    {
                        self::refreshTiket( false );
                    }
                    return true;
                }
                else
                {
                    throw new CmdException( self::INVALID_KEY, "Error: Token invalid." );
                }
            }
            return false;
        } 
        catch ( Throwable $t ) 
        {
            self::entity()->invalidate();
            throw $t;
        }
    }
    
    /**
     * 
     */
    static public function refreshTiket( bool $setHeader = true )
    {
        $expire = new DateTime( 'now' );
        $expire->add( new DateInterval( 'PT'.UIServer::config()->securityTimeout().'M' ) );
        self::entity()->ticket()->expire( $expire->format( DateTime::ATOM ) );
        
        if( $setHeader ) 
        {
            self::entity()->ticket()->token( UUID::generate() );
            UIServer::response()->setAsSecureResponse();
        }
        
        self::entity()->sincronize();
    }
    
    /**
     * 
     * @return string
     */
    static public function toLog(): string
    {
        return json_encode( self::entity(), JSON_PRETTY_PRINT );
    }
    
}
?>