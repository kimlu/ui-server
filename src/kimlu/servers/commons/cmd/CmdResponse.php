<?php
namespace kimlu\servers\commons\cmd;

use JsonSerializable;
use Throwable;
use stdClass;
use kimlu\manager\files\Json;
use kimlu\servers\modules\FileDispatchModule;
use kimlu\servers\modules\Security;
use kimlu\utiles\Text;

class CmdResponse implements JsonSerializable
{

    /**
     * 
     * @var string
     */
    private $fileRelativePath = null;
    
    /**
     * 
     * @var mixed
     */
    private $content = null;
    
    /**
     * 
     * @var Throwable
     */
    private $error = null;
    
    /**
     * 
     * @var array
     */
    private $headers = [];
    
    /**
     * 
     * @var boolean
     */
    private $isSecure = false;
    
    /**
     * 
     */
    public function setAsSecureResponse()
    {
        $this->isSecure = TRUE;
    }
    
    /**
     * 
     * @param string $headerDefinition
     */
    public function setHeader( string $headerDefinition )
    {
        $this->headers[] = $headerDefinition;
    }
    
    /**
     * 
     * @param Throwable $e
     * @return string
     */
    public function error( Throwable $e = null ) : Throwable
    {
        if ( isset( $e ) )
        {
            $this->error = $e;
        }
        return $this->error;
    }
    
    /**
     * 
     * @param string $path
     * @return string
     */
    public function fileRelativePath( string $path = null ) : string
    {
        if ( isset( $path ) )
        {
            $this->fileRelativePath = $path;
        }
        return $this->fileRelativePath;
    }
    
    /**
     * 
     * @param mixed $cntnt
     * @return mixed
     */
    public function content( $cntnt = null )
    {
        if ( isset( $cntnt ) )
        {
            $this->content = $cntnt;
        }
        return $this->content;
    }
    
    /**
     * 
     */
    public function json()
    {
        $kimlu_headers = 'Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Kimlu-Token, Kimlu-Ticket';
        header( 'Access-Control-Allow-Origin: *' );
        foreach ( $this->headers as $headerDefinition ) 
        {
            header( $headerDefinition );
        }
        if ( isset( $this->error ) )
        {
            header('HTTP/1.1 500 Internal Server Error');
            header( 'Content-Type: '.Json::CONTENT_TYPE );
            print json_encode( $this->createErrorModel() );
        }
        else
        {
            header( $kimlu_headers );
            header( 'Content-Type: '.Json::CONTENT_TYPE );
            if( $this->isSecure )
            {
                print '{ "ticket": "'.Security::entity()->ticket()->token().'", "content": ';
            }
            if ( isset( $this->fileRelativePath ) )
            {
                FileDispatchModule::fileManager()->print( $this->fileRelativePath() );
            }
            else
            {
                print json_encode( $this->content );
            }
            if( $this->isSecure )
            {
                print '}';
            }
        }
    }
    
    public function jsonSerialize ()
    {
        $json = new stdClass();
        $json->classname = get_called_class();
        if ( isset( $this->content ) )
        {
            $json->content = $this->content;
        }
        if ( isset( $this->error ) )
        {
            $json->error = json_encode( $this->createErrorModel() );
        }
        if ( $this->fileRelativePath )
        {
            $json->fileRelativePath = $this->fileRelativePath;
        }
        return $json;
    }    
    
    /**
     * 
     * @return stdClass
     */
    private function createErrorModel() : stdClass
    {
        $classname = get_class( $this->error );
        $error = new stdClass();
        $error->type = 'exception';
        $error->classname = Text::create( $classname )->lastPart( '\\' );
        $error->message = $this->error->getMessage();
        $error->id = ( $classname == CmdException::class ) ? $this->error->getId() : $this->error->getCode();
        return $error;
    }
    
    /**
     * 
     * @return string
     */
    public function __toString(): string
    {
        return json_encode( $this->jsonSerialize(), JSON_PRETTY_PRINT );
    }
    
    
}