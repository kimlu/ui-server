<?php
namespace kimlu\servers\commons\cmd;

use Exception;
use Throwable;

/**
 * 
 * @author Máximo
 *
 */
class CmdException extends Exception
{
    /**
     * 
     * @var $id string
     */
    private $id = null;

    /**
     * 
     * @return string
     */
    public function getId() : string
    {
        if ( is_null( $this->id ) )
        {
            $this->id = md5( date( DATE_RFC3339_EXTENDED ) );
        }
        return $this->id;
    }

    /**
     * 
     * @param string $id
     * @param string $message
     * @param Throwable $previousException
     */
    public function __construct( string $id,  string $message, Throwable $previousException = null )
    {
        parent::__construct( $message, 0, $previousException );
        $this->id = $id;
    }

}
