<?php
namespace kimlu\servers\commons\cmd;

use JsonSerializable;
use stdClass;
use kimlu\utiles\Text;

/**
 *
 * @author Máximo
 *        
 */
class CmdRequest implements JsonSerializable
{
    /**
     * 
     * @var CmdRequest
     */
    static private $instance = NULL;
    
    /**
     * 
     * @return CmdRequest
     */
    static public function getInstance() : CmdRequest
    {
        if ( is_null( self::$instance ) )
        {
            self::$instance = new CmdRequest();
        }
        return self::$instance;
    }    

    /**
     * 
     * @var string
     */
    private $cmd = '';
    
    /**
     *
     * @var string
     */
    private $classname = '';
    
    /**
     *
     * @var string
     */
    private $method = '';
    
    /**
     * 
     * @var array
     */
    private $args = [];
    
    private function __construct()
    {
        
        if ( $this->isAsynchronous() ) 
        {
            $input = file_get_contents( 'php://input' );
            $request = json_decode( $input, TRUE );
            $this->cmd = str_replace( '.' , '\\', $request[ 'cmd' ] );
            $this->args = $request[ 'args' ];
            list( $this->classname, $this->method ) = explode( '::', $this->cmd );
        }
        else
        {
            if ( isset( $_SERVER[ 'QUERY_STRING' ] ) && !empty( $_SERVER[ 'QUERY_STRING' ] ) )
           {
               $query = $_SERVER[ 'QUERY_STRING' ];
               if ( Text::create( $query )->contains( '{' )  && Text::create( $query )->endWith( '}' ) )
               {
                   $this->cmd = Text::create( $query )->firstPart( '{' );
                   $this->args = json_decode( urldecode( Text::create( $query )->lastPart( '{', TRUE ) ), TRUE );
               }
               else 
               {
                   $this->cmd = $query; 
                   $this->args = [];
               }
               list( $this->classname, $this->method ) = explode( '::', $this->cmd );
           }
           else
           {
               $this->cmd = 'echo';
               $this->args = [];
           }
        }
    }
    
    /**
     * 
     * @return bool
     */
    public function isAsynchronous() : bool
    {
        return  isset( $_SERVER[ 'HTTP_X_REQUESTED_WITH' ] ) 
                            && strtolower( $_SERVER[ 'HTTP_X_REQUESTED_WITH' ] ) == 'xmlhttprequest'; 
    }
    
    /**
     * 
     * @return bool
     */
    public function isSynchronous() : bool
    {
        return  !$this->isAsynchronous();
    }
    
    /**
     * 
     * @return string
     */
    public function cmd() : string
    {
        return $this->cmd;
    }

    public function args() : array
    {
        return $this->args;
    }
    
    public function classname() : string
    {
        return $this->classname;
    }
    
    public function method() : string
    {
        return $this->method;
    }
    
    public function cmdExist() : bool
    {
        return class_exists( $this->classname ) && method_exists($this->classname, $this->method );
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see JsonSerializable::jsonSerialize()
     */
    public function jsonSerialize ()
    {
        $json = new stdClass();
        $json->cmd = $this->cmd();
        $json->args = $this->args;
        return $json;       
    }

    /**
     * 
     * @return string
     */
    public function __toString(): string 
    {
        return json_encode( $this->jsonSerialize(), JSON_PRETTY_PRINT );
    }
    
}