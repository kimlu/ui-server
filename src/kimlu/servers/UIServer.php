<?php
namespace kimlu\servers;

use ReflectionMethod;
use Throwable;
use kimlu\environment\Env;
use kimlu\servers\commons\cmd\CmdException;
use kimlu\servers\commons\cmd\CmdRequest;
use kimlu\servers\commons\cmd\CmdResponse;
use kimlu\servers\entities\UIServerConfiguration;
use kimlu\servers\modules\FileDispatchModule;
use kimlu\servers\modules\Security;

/**
 *
 * @author Máximo
 *        
 */
abstract class UIServer
{
    
    /**
     * 
     * @var UIServerConfiguration
     */
    static private $config = null;
    
    /**
     * 
     * @var CmdRequest
     */
    static private $request = null;
    
    /**
     * 
     * @var CmdResponse
     */
    static private $response = null;
    
    /**
     * 
     * @return UIServerConfiguration
     */
    static public function config() : UIServerConfiguration
    {
        return self::$config;
    }

    /**
     * 
     * @param string $relativeFilePathConfiguration
     */
    static public function start( string $relativeFilePathConfiguration )
    {
        self::$config = new UIServerConfiguration( Env::fullPathSafe( $relativeFilePathConfiguration ) );
        FileDispatchModule::connect( Env::fullPathSafe( self::config()->repositoryPath() ) );
    }
    
    /**
     * 
     * @return CmdRequest
     */
    static public function request() : CmdRequest
    {
        return self::$request;
    }
    
    /**
     * 
     * @return CmdResponse
     */
    static public function response() : CmdResponse
    {
        return self::$response;
    }
    
    /**
     * 
     */
    static public function execute() 
    {
        ob_start();
        self::$request = CmdRequest::getInstance();
        self::$response = new CmdResponse();
        try
        {
            if ( class_exists( self::$request->classname() ) && method_exists( self::$request->classname(), self::$request->method() ) )
            {
                if ( Security::isConnected() )
                {
                    $method = new ReflectionMethod( self::$request->cmd() );
                    $method->invoke( null , self::$request->args() );
                }
                else 
                {
                    throw new CmdException( 'server', "Error: Request invalid." );
                }
            }
            else
            {
                if ( self::$request->cmd() == 'echo' )
                {
                    self::$response->content( self::$request );
                }
                else
                {
                    throw new CmdException( 'server', "Error: Request not found." );
                }
            }
        }
        catch( Throwable $t )
        {
            Security::entity()->invalidate();
            self::response()->error( $t );
        }
        ob_clean();
        self::$response->json();
        ob_end_flush();
    }
     
}