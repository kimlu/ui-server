<?php
namespace kimlu\servers\models;
use kimlu\entities\models\JsonModel;

/**
 *
 * @author Máximo
 *        
 */
class TicketModel extends JsonModel
{
    
    /**
     * @method token
     * @param string $token_value. Default NULL
     * @return string
     */
    public function token ( string $token_value = NULL ) : string 
    {
        if ( isset( $token_value ) ) 
        { 
            $this->data()->token = $token_value; 
        }
        if ( isset( $this->data()->token ) )
        {
            return $this->data()->token;
        }
        return '';
    }
    
    /**
     * @method expire
     * @param string $expire_value. Default NULL
     * @return string
     */
    public function expire ( string $expire_value = NULL ) : string 
    {
        if ( isset( $expire_value ) ) 
        { 
            $this->data()->expire = $expire_value; 
        }
        if ( isset( $this->data()->expire ) )
        {
            return $this->data()->expire;
        }
        return '';
    }
    
}

