<?php
namespace kimlu\servers\entities;
use kimlu\entities\JsonEntity;

/**
 *
 * @author Máximo
 *        
 */
class UIServerConfiguration extends JsonEntity
{
    
    /**
     * @method securityTimeout
     * @param string $securityTimeout_value. Default NULL
     * @return string
     */
    public function securityTimeout ( string $securityTimeout_value = NULL ) : string 
    {
        if ( isset( $securityTimeout_value ) ) 
        { 
            $this->data()->security->timeout = $securityTimeout_value; 
        }
        if ( isset( $this->data()->security->timeout ) )
        {
            return $this->data()->security->timeout;
        }
        return '';
        
    }

    /**
     * @method repositoryPath
     * @param string $repository_value. Default NULL
     * @return string
     */
    public function repositoryPath ( string $repository_value = NULL ) : string 
    {
        if ( isset( $repository_value ) ) 
        { 
            $this->data()->repository->path = $repository_value; 
        }
        if ( isset( $this->data()->repository->path ) )
        {
            return $this->data()->repository->path;
        }
        return '';
    }
    
}