<?php
namespace kimlu\servers\entities;
use kimlu\entities\JsonEntity;

/**
 *
 * @author Máximo
 *        
 */
class OAuthEntity extends JsonEntity
{
    
    /**
     * @method key
     * @param string $key_value. Default NULL
     * @return string
     */
    public function key ( string $key_value = NULL ) : string 
    {
        if ( isset( $key_value ) ) 
        { 
            $this->data()->key = $key_value; 
        }
        if ( isset( $this->data()->key ) )
        {
            return $this->data()->key;
        }
        return '';
    }    
}