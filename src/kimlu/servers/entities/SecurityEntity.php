<?php
namespace kimlu\servers\entities;
use stdClass;
use kimlu\entities\JsonEntity;
use kimlu\servers\models\TicketModel;

/**
 *
 * @author Máximo
 *        
 */
class SecurityEntity extends JsonEntity
{

    /**
     * @method token
     * @param string $token_value. Default NULL
     * @return string
     */
    public function token ( string $token_value = NULL ) : string 
    {
        if ( isset( $token_value ) ) 
        { 
            $this->data()->token = $token_value; 
            $this->sincronize();
        }
        if ( !isset( $this->data()->token ) )
        {
            $this->data()->token = '';
        }
        return $this->data()->token;
    }

    /**
     * @method ticket
     * @param TicketModel $cmdTicket_value
     * @return TicketModel
     */
    public function ticket ( TicketModel $ticket_value = NULL ) : TicketModel 
    {
        if ( isset( $ticket_value ) ) 
        { 
            $this->data()->ticket = $ticket_value; 
        }
        if ( !isset( $this->data()->ticket ) )
        {
            $ticket = new stdClass();
            $ticket->token = '';
            $ticket->time = '';
            $this->data()->ticket = new TicketModel( $ticket );
        }
        if ( get_class( $this->data()->ticket ) == stdClass::class )
        {
            $this->data()->ticket = new TicketModel( $this->data()->ticket );
        }
        return $this->data()->ticket;
    }    
    
    /**
     * 
     */
    public function invalidate()
    {
        $this->token( 'INVALID' );
        $this->ticket()->token( 'INVALID' );
        $this->ticket()->expire( 'INVALID' );
        $this->sincronize();
    }
    
}